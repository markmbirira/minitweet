## minitweet
> A twitter clone (or something close) in Slim PHP, OOP PHP and JQuery
>Uses twig views for smoother templating

### Installation
- Ensure you have PHP's Composer installed
- or get it from [Composer Website](https://getcomposer.org/download/)
- Install Dependencies with
```
  composer install
```
- Create a database ``slimproject``
- Edit the config file at assets/config.php with 
- your correct database credentials

- Start you development server inside the public/ folder with
```
  php ---server localhost:8000
```

- Navigate to http://localhost:8000
