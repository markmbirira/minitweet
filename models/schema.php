<?php

class Minitweet {

  private $db;
  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function user() {
    $sql = "CREATE TABLE IF NOT EXISTS users (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            username VARCHAR(100) NOT NULL UNIQUE,
            email VARCHAR(100) NULL,
            password VARCHAR(255) NOT NULL,
            memberSince DATETIME NOT NULL
    )";

    $conn = $this->db->prepare($sql);
    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    echo "Table users created successfully...\t\t\t\t\t\t\t 2 remaining\n";

  }

  public function article() {
    $sql = "CREATE TABLE IF NOT EXISTS feeds (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            body VARCHAR(100) NOT NULL,
            timePosted DATETIME NOT NULL, # DEFAULT NOW(),
            status SMALLINT NOT NULL,
            author VARCHAR(100) NOT NULL,
            path VARCHAR(255) NULL,
            type VARCHAR(100) NULL,
            votes SMALLINT NULL,
            down_votes SMALLINT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    }  catch(PDOException $e) {
      echo $e->getMessage();
    }

    echo "Table blogs created successfully...\t\t\t\t\t\t\t 1 remaining\n";

  }

  public function comment() {
    $sql = "CREATE TABLE IF NOT EXISTS comments (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            comment VARCHAR(255) NOT NULL,
            timePosted DATETIME NOT NULL, # DEFAULT NOW(),
            author VARCHAR(100) NOT NULL,
            article_id INT NOT NULL
    )";

    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
    } catch (PDOException $e) {
      echo $e->getMessage();
    }

    echo "Table comments created successfully...\t\t\t\t\t\t\t 0 remaining\n";

  }

}

include "config.php";
$cbl = new Minitweet($db);

$cbl->user();
$cbl->article();
$cbl->comment();

?>
