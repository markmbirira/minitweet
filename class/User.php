<?php

class User {
  private $db;

  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function register($username, $email, $password) {

  	try {
  		$sql = "INSERT INTO users (username,email, password, memberSince) values (:username,:email, :password, NOW())";
  		$conn = $this->db->prepare($sql);
  		$conn->bindValue(":username", $username, PDO::PARAM_STR);
      $conn->bindValue(":email", $email, PDO::PARAM_STR);
  		$conn->bindValue(":password", hash('SHA256', $password), PDO::PARAM_STR);

  		$result = $conn->execute();
      return $result;
    }  catch(PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function user_exists($username) {
    $sql = " SELECT username FROM users WHERE username = :username LIMIT 1";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":username",$username,PDO::PARAM_STR);
    $conn->execute();
    return $conn->fetchColumn();

  }

  public function login($username, $password) {
  	try {
      $sql = "SELECT username, password FROM users where username = :username AND password = :password LIMIT 1";
      $conn = $this->db->prepare($sql);

      $conn->bindValue(":username", $username, PDO::PARAM_STR);
      $conn->bindValue(":password", hash('SHA256',$password), PDO::PARAM_STR);

      $conn->execute();
      $result = $conn->fetchColumn(); /* returns false on failure and the table row on success */
      return $result;

    }  catch (PDOException $e) {
    	$e->getMessage();
    }
  }

}


?>
