<?php

# routes for articles
$app->group('/', function () use ($app) {
  $app->get('', function ($request, $response) {
    $article = new Article($this->db);
    $status = 1;
    $data = $article->read($status);
    return $this->view->render($response, 'home.html',['data' => $data]);
    // echo json_encode($data);

  });

  $app->get('readone/{id}', function ($request, $response,$args) {
    $article = new Article($this->db);
    $id = $args['id'];
    $data = $article->readone($id);
    return $this->view->render($response, 'home.html', ['data' => $data]);
    // echo json_encode($data);
  });

  $app->group('create', function () use ($app) {
    $app->get('', function ($request, $response) {
      $this->logger->addInfo("Link for a new post");
      return $this->view->render($response, 'create.html');
    });

    $app->post('/new', function ($request, $response) {
      $this->logger->addInfo('Log whatever here!');
      $data = $request->getParsedBody();
      $files = $request->getUploadedFiles();

      $body = filter_var($data['article'], FILTER_SANITIZE_STRING);
      $author = filter_var($data['author'], FILTER_SANITIZE_STRING);
      $status = 1;
      #uploaded file
        $file = $files['file'];
        if ($file->getError() === UPLOAD_ERR_OK or NULL) {
          $file_name = $file->getClientFilename();
          $type = $file->getClientMediaType();
          $path = "/uploads/$file_name";
          $file->moveTo("uploads/$file_name");
        } else {
          $type = null;
          $path = null;
        }
    
      // die();
      $article = new Article ($this->db);
      $article->create($body,$author,$status,$path,$type);

      # redirect after inserting
      $message = $this->flash->addMessage('article', 'Post added successfully');

      return $response->withStatus(302)
                      ->withHeader('Location', '/');
    });
  }); # ending group 'create'

  $app->group('update', function () use($app) {
    $app->post('', function ($request, $response) {
      $article = new Article($this->db);
      $data = $request->getParsedBody();

      $id = (int)$data['id'];
      $body = $data['body'];

      var_dump($id);
      var_dump($body);

      $article->update($id, $body);

      # redirect after inserting
      return $response->withRedirect('/readone/'.$id, 302);
    });
    $app->get('/{id}', function ($resquest, $response, $args) {
      $article = new Article($this->db);
      # collect data to populate the form with
      $id = $args['id'];
      $data = $article->readOne($id);

      $body = $data[0]['body'];
      return $this->view->render($response, 'update.html', ["id" => $id, "body" => $body]);
    });
  });

  $app->get('delete/{id}', function ($request, $response, $args) {
    $article = new Article($this->db);
    $id = $args['id'];
    $status = 0;

    $article->delete($id, $status);

    # redirect after inserting
    return $response->withRedirect('/', 302);
  });

  # liking and disliking
  $app->get('like/{id}', function ($request, $response, $args) {
    
  });

  $app->get('dislike/{id}', function ($request, $response, $args) {
    
  });

});

# user management groups

$app->group('/auth/', function () use($app) {
  $app->group('login', function () use ($app) {
    $app->get('', function($request, $response) {
      return $this->view->render($response, 'auth/login.html');
    });
    $app->post('', function ($request, $response) {
      $user = new User($this->db);
      $data = $request->getParsedBody();
      $username = $data['username'];
      $password = $data['password'];

      # log the user in
      $login = $user->login($username, $password);
      if ($login) {
        $_SESSION['username'] = $username;
        return $response->withRedirect('/', 302);
      } else {
        # flash message
        return $this->view->render($response, 'auth/login.html');
      }
    });
  });

  $app->group('signup', function () use ($app) {
    $app->get('', function ($request, $response) {
      return $this->view->render($response, 'auth/signup.html');
    });

    $app->post('', function ($request, $response, $args) {
      $user = new User($this->db);
      $data = $request->getParsedBody();

      $username = $data['username'];
      $email = $data['email'];
      $password = $data['password'];
      $password2 = $data['password2'];

      #validate
      if ($password == $password2) {
        $password = $password2;
      } else {
        return $this->view->render($response, 'auth/signup.html');
      }

      # check if user exists
      $exists = $user->user_exists($username);
      if (!$exists) {
        $user->register($username, $email, $password);
        # session
        $_SESSION['username'] = $username;
        # redirect to home
        return $response->withRedirect('/', 302);
      } else {
        # flash message for failure
        return $this->view->render($response, 'auth/signup.html');
      }

    });
  }); # closing signup management

}); # closing user management route group


?>
